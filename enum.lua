function enum(name)
    local enumTbl = {}

    return function (tbl)
        if tbl == nil then
            return enumTbl
        end

        local idx = 1

        for k, v in pairs(tbl) do
            enumTbl[k] = setmetatable({parent = enumTbl, name = v, idx = idx}, {
                __tostring = function (self)
                    return string.format("enum %s.%s", name, self.name)
                end
            })
            idx = idx + 1
        end
    end

    _G[name] = enumTbl
end
